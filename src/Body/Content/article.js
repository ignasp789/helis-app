import React, { Component } from 'react';

class Article extends Component {
    render() {
        return (
            <div className="article-block">
                <div className="article-inner-block">  
                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>                  
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Aliquam sem tortor, mollis in enim at, vulputate pretium erat. Vivamus luctus condimentum dictum. 
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis est et tellus vehicula mollis. 
                        Maecenas nec tellus ut leo varius luctus eu eget nulla. Proin mauris elit, ullamcorper eget nisi non, auctor ullamcorper sapien. 
                       </p>
                    <p>Curabitur lacus risus, pellentesque a feugiat quis, tincidunt vitae velit.
                         Maecenas tempus in libero nec tempor. Nullam viverra lorem vel mauris hendrerit volutpat. 
                         Maecenas gravida est diam. Proin accumsan ipsum vel dui mattis, sit amet tempor nunc tempor. 
                         Nunc varius lacus iaculis nisl dapibus, sit amet elementum orci commodo. Donec efficitur mauris urna, et consequat mi tincidunt non. Fusce quis dapibus turpis, nec vehicula elit.
                       </p>
                    <p>Praesent iaculis neque lorem, ut suscipit enim dictum non. 
                        Duis vestibulum est ex, nec dapibus magna commodo eget. 
                        Nunc molestie mauris ultricies neque euismod laoreet. 
                        Aliquam dignissim luctus magna, sed interdum justo congue id. 
                        Duis finibus eget est a dapibus.</p>
                    <p>Mauris eget pulvinar leo. Sed vel condimentum enim. 
                        Aliquam erat volutpat. Integer fringilla felis in condimentum aliquam. Vivamus ac tortor purus. 
                        Phasellus vel convallis arcu. Fusce maximus nec nibh id volutpat. Praesent bibendum ultrices tempus.</p>
                </div>
            </div>
        );
    }
}

export default Article;