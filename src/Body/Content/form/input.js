import React, { Component } from 'react';
import $ from 'jquery';

class Input extends Component {
    constructor() {
        super();
    }

    handleNameChange(event) {
        let inputValue = event.target.value;
        if (inputValue) {
            let regex = /^[a-zA-Z ]+$/;
            if (!regex.test(inputValue)) {
                let errorMessage = $('#name-error');
                errorMessage.removeClass('invisible-error');
                errorMessage.addClass('error');

            }
            else {
                let errorMessage = $('#name-error');
                errorMessage.removeClass('error');
                errorMessage.addClass('invisible-error');
            }
        }

    }

    handleLastNameChange(event) {
        let inputValue = event.target.value;
        if(inputValue){
        let regex = /^[a-zA-Z ]+$/;
        if (!regex.test(inputValue)) {
            let errorMessage = $('#last-name-error');
            errorMessage.removeClass('invisible-error');
            errorMessage.addClass('error');

        }
        else {
            let errorMessage = $('#last-name-error');
            errorMessage.removeClass('error');
            errorMessage.addClass('invisible-error');
        }
    }
    }

    submit(event) {
        if($('.error').length)
        {
          event.preventDefault();
        }
    }

    render() {
        return (
            <div className="input-box">
                <form action="/" onSubmit={this.submit}>
                <div className="input-wrapper">
                    <div className="input">
                        <span>First name</span>
                        <input type="text" placeholder="Your name here" onBlur={this.handleNameChange} required></input>
                        <span id="name-error" className="invisible-error">Use only English letters!</span>
                    </div>
                    <div className="input">
                        <span>Last name</span>
                        <input type="text" placeholder="Your last name here" onBlur={this.handleLastNameChange} required></input>
                        <span id="last-name-error" className="invisible-error">Use only English letters!</span>
                    </div>
                    <div className="input" >
                        <span>Your message</span>
                        <textarea rows="4" placeholder="Type your message here" required>
                        </textarea>
                    </div>
                    <button className="btn">Submit</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default Input;