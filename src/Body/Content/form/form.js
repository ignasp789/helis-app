import React, { Component } from 'react';
import Input from './input';

class Form extends Component {
    render() {
        return (
            <div className="form-block">
                <div className="form-block-text">
                <h2>Quisque a justo</h2>
                <hr/>
                <p>Mauris eget pulvinar leo. Sed vel condimentum enim. Aliquam erat volutpat. 
                Integer fringilla felis in condimentum aliquam.</p>
                </div>
                 <Input/>
            </div>            
        );
    }
}

export default Form;