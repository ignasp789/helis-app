import React, { Component } from 'react';

class DescriptionBlock extends Component {
    render() {
        return (
            <div className="descriptions-block">
                {this.props.text}
            </div>
        );
    }
}

export default DescriptionBlock;