import React, { Component } from 'react';
import DescriptionBlock from './descriptionBlock';

class Descriptions extends Component {
    render() {
        return (
            <div className="descriptions">
                <div className="descriptions-wrapper">
                <DescriptionBlock text="Donec et diam et eros accumsan. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nulla purus, efficitur sed dictum eget, euismod non mauris."/>
                <DescriptionBlock text="Vestibulum sed ex a nisi iaculis cursus ac placerat est. Fusce aliquet porta molestie. Etiam ac lorem quis nisi dapibus maximus. Curabitur molestie turpis."/>
                <DescriptionBlock text="Proin metus urna, imperdiet posuere mattis in, tempor at tellus. Mauris ac ullamcorper arcu, et sodales leo. Duis faucibus vel ligula at commodo."/>                             
                </div>
            </div>
        );
    }
}

export default Descriptions;