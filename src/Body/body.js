import React, { Component } from 'react';
import Article from './Content/article';
import Form from './Content/form/form';
import Descriptions from './Content/descriptions/descriptions';

class Body extends Component {
    render() {
        return (
            <div className="container">     
                <div className="row">       
                <Article/>
                <Form/>
                </div>
                <div className="row">  
                <Descriptions/>
                </div>
            </div>
        );
    }
}

export default Body;