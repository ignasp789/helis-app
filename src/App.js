import React, { Component } from 'react';
import logo from './Pictures/TestSite_logo@1X.png';
import './App.css';
import MenuBar from './menuBar/menu-bar';
import Body from './Body/body';
import Footer from './Footer/footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <MenuBar/>
        </header>
        <Body/>
        <Footer/>
      </div>
    );
  }
}

export default App;
