import React, { Component } from 'react';
import Dropdown from './dropdownMenu/dropdown'
class MenuBar extends Component {
    render() {
      return (
          <div className="menu-bar">                               
              <a className="menu-item">Projects</a>    
              <a className="menu-item">About us</a>
              <a className="menu-item">Testimonials</a>
              <a className="last-menu-item">Contacts</a>
          </div>
      );
    }
  }

  export default MenuBar;