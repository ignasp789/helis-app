import React, { Component } from 'react';
import DropdownMenu from 'react-dd-menu';

class Dropdown extends React.Component {
    constructor() {
      super();
      this.state = {
          isMenuOpen: false
      };
      this.click = this.click.bind(this);
      this.toggle = this.toggle.bind(this);
      this.close = this.close.bind(this);
    }
  
    toggle() {
      this.setState({ isMenuOpen: !this.state.isMenuOpen });
    }
  
    close() {
      this.setState({ isMenuOpen: false });
    }
  
    click() {
      console.log('You clicked an item');
    }
  
    render() {
      const menuOptions = {
        isOpen: this.state.isMenuOpen,
        close: this.close,
        toggle: <a onClick={this.toggle} className="menu-item">Projects</a>,
        // align: 'right'
      };
      return (
        <DropdownMenu {...menuOptions}>  
        </DropdownMenu>
      );
    }
  }

  export default Dropdown;