import React, { Component } from 'react';

class FooterMenuBar extends Component {
    render() {
        return (
            <div className="footer-menu-bar">    
              <a>• Home</a>                 
              <a>• Projects</a>                                 
              <a>• About us</a>
              <a>• Testimonials</a>
              <a>• Contacts</a>                 
            </div>
        );
    }
}

export default FooterMenuBar;