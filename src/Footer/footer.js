import React, { Component } from 'react';
import FooterMenuBar from './footerMenuBar';
import FooterTextBlock from './footerTextBlock'

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <div className="footer-content">
                    <FooterMenuBar />
                    <FooterTextBlock/>
                </div>
                </div>
                );
    }
}

export default Footer;