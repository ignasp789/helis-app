import React, { Component } from 'react';

class FooterTextBlock extends Component {
    render() {
        return (
            <div className="footer-text-block">
                Cum sint inermis at, ex pro illum commodo. Copiosae efficiendi sea ea, per no quaeque feugiat. Scripta evertitur qui ne.
                Ne vis natum delenit lucilius, vix elit graeco intellegebat eu. Volumus nostrum qui et, at duo eius ornatus vivendum.
            </div>
        )
    }
}

export default FooterTextBlock;